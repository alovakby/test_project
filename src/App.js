import {PieContainer} from "./components/PieContainer";

function App() {
  return (
    <div className="App">
      <PieContainer/>
    </div>
  );
}

export default App;
