import React, { useRef } from "react";
import { Cell, LabelList, Pie, PieChart, Tooltip } from "recharts";

import { data01, data02, COLORS } from "./data";

export const PieContainer = () => {
  const positionRef = useRef(null);

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = (props) => {
    const radius =
      props.innerRadius + (props.outerRadius - props.innerRadius) * 2.5;
    const x = props.cx + radius * Math.cos(-props.midAngle * RADIAN);
    let y = props.cy + radius * Math.sin(-props.midAngle * RADIAN);
    console.log(props.index);
    if (positionRef.current === null) {
      positionRef.current = y;
    } else if (y - positionRef.current > -20) {
      y -= 25;
      console.log("y", props.index, y);
      console.log("current", positionRef.current);
    } else if (positionRef.current - y > -20) {
      y += 25;
      console.log("y", props.index, y);
      console.log("current", positionRef.current);
    }

    if (props.index === data02.length - 1) {
      positionRef.current = null;
    } else {
      positionRef.current = y;
    }

    console.log("Hello");
    console.log("Hello world");
    console.log("Hello world 1");

    console.log(1);
    console.log(2);
    console.log(3);

    return (
      <text
        x={x}
        y={y}
        fill="black"
        textAnchor={x > props.cx ? "start" : "end"}
        dominantBaseline="central"
      >
        {`${props.index} - ${y}`}
      </text>
    );
  };

  console.log("render");

  return (
    <PieChart width={730} height={300}>
      <Tooltip />
      <Pie
        data={data01}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        outerRadius={50}
        fill="#8884d8"
      >
        {data01.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
      <Pie
        data={data02}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        innerRadius={60}
        outerRadius={80}
        label={renderCustomizedLabel}
        labelLine={false}
      >
        {data02.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
        ))}
      </Pie>
    </PieChart>
  );
};
