export const data01 = [
  {
    name: "Отклонено",
    value: 400,
  },
  {
    name: "Принято",
    value: 300,
  },
];
export const data02 = [
  {
    name: "Group A",
    value: 2400,
  },
  {
    name: "Group B",
    value: 456,
  },
  {
    name: "Group C",
    value: 139,
  },
  {
    name: "Group D",
    value: 980,
  },
  {
    name: "Group E",
    value: 390,
  },
  {
    name: "Group F",
    value: 480,
  },
  {
    name: "Group G",
    value: 200,
  },
  {
    name: "Group H",
    value: 100,
  },
  {
    name: "Group I",
    value: 150,
  },
  {
    name: "Group J",
    value: 170,
  },
  {
    name: "Group K",
    value: 50,
  },
  {
    name: "Group L",
    value: 30,
  },
];

export const COLORS = ["#0088FE", "#00C49F", "#FFBB28", "#FF8042"];
